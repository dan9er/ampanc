/*  AmPanc
    Copyright (C) 2021  dan9er

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <3ds.h>

int main(int argc, char* argv[])
{
    //------//
    // DATA //
    //------//

    // UTF-8 url buffer
    static char urlStr[1025];

    // consoles
    static PrintConsole topConsole   ,
                        bottomConsole;

    // software keyboard
    static SwkbdState swkbd;

    //------//
    // INIT //
    //------//

    // init gfx
    gfxInitDefault();

    // enable Hori-HD if not on O2DS
    {
        u8 model;
        CFGU_GetSystemModel(&model);
        if (model != 3)
            gfxSetWide(true);
    }

    // init bottom screen
    consoleInit(GFX_BOTTOM, &bottomConsole);

    // print bottom text (constant)
    printf("AmPanc indev\n");
    printf("by dan9er (https://d9r.weebly.com)\n\t& contributors\n\n");
    printf("Press A to enter URL\n\n");
    printf("Licensed under GNU GPL v3\n\t(https://gnu.org/licenses/gpl-3.0)\n");
    printf("There is NO warranty; not even for\n\tMERCHANTABILITY or FITNESS FOR A\n\tPARTICULAR PURPOSE.\n");
    printf("Source: https://gitlab.com/dan9er/ampanc");

    // init top screen and switch to it
    consoleInit(GFX_TOP, &topConsole);
    consoleSelect(&topConsole);

    //--------//
    // UPDATE //
    //--------//

    while (aptMainLoop())
    {
        // read buttons
        hidScanInput();
        u32 kDown = hidKeysDown();

        // START: gtfo
        if (kDown & KEY_START)
            break;

        // A: ask for URL and make request
        if (kDown & KEY_A)
        {
            swkbdInit(&swkbd, SWKBD_TYPE_NORMAL, 2, 1028);
            swkbdSetValidation(&swkbd, SWKBD_NOTEMPTY_NOTBLANK, 0, 0);
            swkbdSetFeatures(&swkbd, SWKBD_DARKEN_TOP_SCREEN);
            swkbdSetInitialText(&swkbd, "gemini://");

            if (swkbdInputText(&swkbd, urlStr, sizeof(urlStr)) == SWKBD_BUTTON_RIGHT)
                printf("%s\n", urlStr); // TODO actually make a request
        }

        // wait for vblank, then flush & swap
        gspWaitForVBlank();
        gfxFlushBuffers();
        gfxSwapBuffers();
    }

    // bye
    gfxExit();
    return 0;
}
