# AmPanc
A Gemini client for the Nintendo 3DS in C.  
[("AmPanc" is an anagram for Pancam, the stereoscopic cameras used on the Spirit and Opportunity rovers)](https://en.wikipedia.org/wiki/Pancam)

## What?
[What is the Gemini protocol? [Gemini]](gemini://gemini.circumlunar.space/)  
[What is the Gemini protocol? [HTTPS]](https://gemini.circumlunar.space/)

## Dependencies
[devkitARM](https://devkitpro.org/wiki/Getting_Started)  
[libctru v2.0.1](https://github.com/devkitPro/libctru/releases/tag/v2.0.1)  
3ds-mbedtls v2.16.6-1

## License
Licensed under GNU GPL 3.0.  
[COPYING.gmi](COPYING.gmi)  
[COPYING.md](COPYING.md)  
